<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package lambdaresto
 */

?>

	</div>

	<footer id="colophon" class="site-footer">
		<div class="container">
			<div class="row">
                <div class="col-sm-4 footerSection">
                    <h3 class="footerHeading">About Us</h3>
                    <img class="footerDivider" src="<?php echo get_template_directory_uri().'/images/ingredients-divider.png'; ?>">
                    <p class="footerBody">Lambda's new and expanded Chelsea location represents a truly authentic Greek patisserie, featuring breakfasts of fresh croissants and steaming bowls of café. Lamda the best restaurant in town.</p>
                </div>
                <div class="col-sm-4 footerSection">
                    <h3 class="footerHeading">Opening Hours</h3>
                    <img class="footerDivider" src="<?php echo get_template_directory_uri().'/images/ingredients-divider.png'; ?>">
                    <p class="footerBody"><span class="footerBoldText">Mon-Thu</span>: 7:00am-8:00pm </p>
                    <p class="footerBody"><span class="footerBoldText">Fri-Sun</span>: 7:00am-10:00pm</p>
                </div>
                <div class="col-sm-4 footerSection">
                    <h3 class="footerHeading">Our Location</h3>
                    <img class="footerDivider" src="<?php echo get_template_directory_uri().'/images/ingredients-divider.png'; ?>">
                    <p class="footerBody footerBoldText">19th Paradise Street Sitia</p>
                    <p class="footerBody footerBoldText">128 Meserole Avenue</p>
                </div>
            </div>
		</div>
	</footer>
</div>

<?php wp_footer(); ?>

</body>
</html>
