<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package lambdaresto
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'lambda-resto' ); ?></a>

	<header class="site-header">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<h1 class="site-name"><?php echo get_bloginfo(); ?></h1>
				</div>
				
				<div class="col-sm-8">
					<nav id="site-navigation" class="main-navigation">
						<?php
							wp_nav_menu( array(
								'menu' => 'Main Menu',
								'menu_class' => 'main-menu'	
							) );
						?>
					</nav>
				</div>
			</div>
		</div>
	</header>

    <header class="mobile-header">
        <img src="<?php echo get_template_directory_uri().'/images/mobile-header-slider.png';?>" class="mobile-header-slider-icon">
        <h1 class="site-name"><?php echo get_bloginfo(); ?></h1>

        <div class="mobile-navbar">
            <?php
                wp_nav_menu( array(
                    'menu' => 'Main Menu',
                    'menu_class' => 'main-menu'	
                ) );
            ?>
        </div>
	</header>

	<div id="content" class="site-content">
