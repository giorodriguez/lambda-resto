<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package lambdaresto
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="promo" id="promo">
				<div class="container">
					<h1 class="heading">the right ingredients for the right food</h1>
					<img src="<?php echo get_template_directory_uri().'/images/divider.png';?>" class="divider">
					<div class="promo-buttons">
						<a href="#reservations" class="bookbtn">Book a table</a>
						<a href="#menu" class="menubtn">See the menu</a>
					</div>
				</div>
			</div>

			<div class="about" id="about">
				<div class="container">
					<div class="row">
						<div class="col-sm-6">
							<h3 class="heading">Just the right food</h3>
							<img src="<?php echo get_template_directory_uri().'/images/about-divider.png';?>" class="about-divider">
							<p class="about-text">If you’ve been to one of our restaurants, you’ve seen – and tasted – what keeps our customers coming back for more. Perfect materials and freshly baked food, delicious Lambda cakes,  muffins, and gourmet coffees make us hard to resist! Stop in today and check us out!</p>
							<img src="<?php echo get_template_directory_uri().'/images/about-cook.png'; ?>" class="about-cook">
						</div>
						<div class="col-sm-6">
							<img src="<?php echo get_template_directory_uri().'/images/about-dish.png'; ?>" class="about-dish">
						</div>
					</div>
				</div>
			</div>

			<div class="ingredients" id="ingredients">
				<div class="container">
					<!-- <div class="row">
						<div class="col-sm-6">  -->
							<div class="ingredients-section">
								<div class="ingredients-text">
									<p class="heading">Fine ingredients</p>
									<img src="<?php echo get_template_directory_uri().'/images/ingredients-divider.png';?>">
									<p class="text">If you’ve been to one of our restaurants, you’ve seen – and tasted – what keeps our customers coming back for more. Perfect materials and freshly baked food, delicious Lambda cakes,  muffins, and gourmet coffees make us hard to resist! Stop in today and check us out!</p>
								</div>

								<div class="ingredients-images">
									<img src="<?php echo get_template_directory_uri().'/images/ingredient-3.png';?>" class="ingredient-image">
									<img src="<?php echo get_template_directory_uri().'/images/ingredient-2.png';?>" class="ingredient-image">
									<img src="<?php echo get_template_directory_uri().'/images/ingredient-1.png';?>" class="ingredient-image">
								</div>
							</div>
						<!-- </div>
					</div> -->
				</div>
			</div>

            <div class="menu" id="menu">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 menu-container">
                            <h3 class="menu-classification">Appetizers</h3>
                            <img src="<?php echo get_template_directory_uri().'/images/menu-divider.png'; ?>" class="menu-divider">
                            <div class="menu-name-and-price">
                                <span class="menu-name">Tzatsiki</span>
                                <span class="menu-price">$ 3.99</span>
                                <div class="menu-border"></div>
                                <p class="menu-description">Refreshing traditional cucumber and garlic yoghurt dip.</p>
                            </div>
                            <div class="menu-name-and-price">
                                <span class="menu-name">Tzatsiki</span>
                                <span class="menu-price">$ 3.99</span>
                                <div class="menu-border"></div>
                                <p class="menu-description">Refreshing traditional cucumber and garlic yoghurt dip.</p>
                            </div>
                        </div>
                        <div class="col-sm-6 menu-container">
                            <h3 class="menu-classification">Starters</h3>
                            <img src="<?php echo get_template_directory_uri().'/images/menu-divider.png'; ?>" class="menu-divider">

                            <div class="menu-name-and-price">
                                <span class="menu-name">Haloumi</span>
                                <span class="menu-price">$ 3.99</span>
                                <div class="menu-border"></div>
                                <p class="menu-description">Refreshing traditional cucumber and garlic yoghurt dip.</p>
                            </div>
                            <div class="menu-name-and-price">
                                <span class="menu-name">Haloumi</span>
                                <span class="menu-price">$ 3.99</span>
                                <div class="menu-border"></div>
                                <p class="menu-description">Refreshing traditional cucumber and garlic yoghurt dip.</p>
                            </div>
                        </div>
                        <div class="col-sm-6 menu-container">
                            <h3 class="menu-classification">Salads</h3>
                            <img src="<?php echo get_template_directory_uri().'/images/menu-divider.png'; ?>" class="menu-divider">

                            <div class="menu-name-and-price">
                                <span class="menu-name">Olive Special</span>
                                <span class="menu-price">$ 5.99</span>
                                <div class="menu-border"></div>
                                <p class="menu-description">Refreshing traditional cucumber and garlic yoghurt dip.</p>
                            </div>
                            <div class="menu-name-and-price">
                                <span class="menu-name">Olive Special</span>
                                <span class="menu-price">$ 5.99</span>
                                <div class="menu-border"></div>
                                <p class="menu-description">Refreshing traditional cucumber and garlic yoghurt dip.</p>
                            </div>
                        </div>
                        <div class="col-sm-6 menu-container">
                            <h3 class="menu-classification">Main Dishes</h3>
                            <img src="<?php echo get_template_directory_uri().'/images/menu-divider.png'; ?>" class="menu-divider">

                            <div class="menu-name-and-price">
                                <span class="menu-name">Fried Chicken</span>
                                <span class="menu-price">$ 5.25</span>
                                <div class="menu-border"></div>
                                <p class="menu-description">Refreshing traditional cucumber and garlic yoghurt dip.</p>
                            </div>
                            <div class="menu-name-and-price">
                                <span class="menu-name">Fried Chicken</span>
                                <span class="menu-price">$ 5.25</span>
                                <div class="menu-border"></div>
                                <p class="menu-description">Refreshing traditional cucumber and garlic yoghurt dip.</p>
                            </div>
                            <div class="menu-name-and-price">
                                <span class="menu-name">Fried Chicken</span>
                                <span class="menu-price">$ 5.25</span>
                                <div class="menu-border"></div>
                                <p class="menu-description">Refreshing traditional cucumber and garlic yoghurt dip.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="reviews" id="reviews">
                <div class="container">
                    <div class="review">
                        <p class="heading">Guest Reviews</p>
                        <img src="<?php echo get_template_directory_uri().'/images/ingredients-divider.png'; ?>">
                        <p class="review-description">If you’ve been to one of our restaurants, you’ve seen – and tasted – what keeps our customers coming back for more. Perfect materials and freshly baked food, delicious Lambda cakes,  muffins, and gourmet coffees make us hard to resist! Stop in today and check us out!</p>
                        <p class="review-author">- food inc, New York</p>
                    </div>
                </div>
            </div>

            <div class="reservations" id="reservations">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 reservationForm">
                            <h3 class="reservationHeading">Just the right food</h3>
                            <img src="<?php echo get_template_directory_uri().'/images/menu-divider.png'; ?>" class="menu-divider">
                            <p class="reservationBody">If you’ve been to one of our restaurants, you’ve seen – and tasted – what keeps our customers coming back for more. Perfect materials and freshly baked food.</p>
                            <p class="reservationBody">Delicious Lambda cakes,  muffins, and gourmet coffees make us hard to resist! Stop in today and check us out! Perfect materials and freshly baked food.</p>

                            <form method="POST" class="reserveForm">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" name="name" class="form-control input" placeholder="your name*">
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" name="email" class="form-control input email" placeholder="your email*">
                                </div>
                                <div class="form-group">
                                    <label>Date</label>
                                    <input type="date" name="name" class="form-control input" placeholder="date*">
                                </div>
                                <div class="form-group">
                                    <label>Table No.</label>
                                    <select name="tableNumber" class="form-control input tableNumber">
                                        <?php for($i=1;$i<=20;$i++) { ?>
                                                    <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <button type="submit" class="reserveBtn">Book now!</button>
                            </form>
                        </div>
                        <div class="col-lg-6 reservationFoodImage">
                            <div class="reservationImageContainer">
                                <img src="<?php echo get_template_directory_uri().'/images/reservation-1.jpg'; ?>" class="reservationImage">
                            </div>
                            <div class="reservationImageContainer">                           
                                <img src="<?php echo get_template_directory_uri().'/images/reservation-2.jpg'; ?>" class="reservationImage">
                            </div>
                            <div class="reservationImageContainer">
                                <img src="<?php echo get_template_directory_uri().'/images/reservation-1.jpg'; ?>" class="reservationImage">
                            </div>
                            <div class="reservationImageContainer">                           
                                <img src="<?php echo get_template_directory_uri().'/images/reservation-2.jpg'; ?>" class="reservationImage">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</main>
	</div>
<?php
get_footer();
