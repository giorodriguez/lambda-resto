$(document).ready(function(){
    $(window).scroll(function(){
        if($(window).scrollTop() > 95) {
            $(".site-header").addClass("fixedHeader");
        } else {
            $(".site-header").removeClass("fixedHeader");
        }
    });

    $(".reserveForm").submit(function(event){
        event.preventDefault();
    });
    
    $(".mobile-navbar").hide();

    $(".mobile-header-slider-icon").click(function(){
        // $(this).attr("class", "mobile-header-close");
        // $(this).attr("src", siteData.templateDirectoryURL+"/images/mobile-header-close.png");
        $(".mobile-navbar").slideToggle();
    });

    // $(".mobile-header-close").click(function(){
    //     $(this).attr("class", "mobile-header-slider-icon");
    //     $(this).attr("src", siteData.templateDirectoryURL+"/images/mobile-header-slider.png");
    //     $(".mobile-navbar").slideToggle();
    // });

    $('.reservationFoodImage').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 2,
        slidesToScroll: 2,
        arrows:false,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: false,
                    arrows:false
                }
            }
        ]
    });
});